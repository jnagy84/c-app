﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CSharpApp
    
{ 
    public partial class Form1 : Form
    {
        byte bCount=0;
        Dots [] dotContainer;
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Random iRand = new Random();
            
            Color cColor;
            switch (iRand.Next(5))
            { case 0:
                    cColor = Color.Red;
                    break;
                case 1:
                    cColor = Color.Blue    ;
                    break;
                case 2:
                    cColor = Color.Green;
                    break;
                case 3:
                    cColor = Color.Purple;
                    break;
                case 4:
                    cColor = Color.Orange;
                    break;
                default:
                    cColor = Color.Yellow;
                    break;

            }

            button2.BackColor = cColor;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bCount++;
            if ( bCount == 3)
            {
                MessageBox.Show("Please Stop Clicking Me!");
                bCount = 0;
            }
                     }
    }
}
